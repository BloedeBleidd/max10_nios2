#ifndef _7SEG_H_
#define _7SEG_H_

#include <stdint.h>

#define SEG_A  0
#define SEG_B  1
#define SEG_C  2
#define SEG_D  3
#define SEG_E  4
#define SEG_F  5
#define SEG_G  6
#define SEG_DP 7

#define MINUS 16
#define OFF 17

void intDisplayDec(uint16_t number);

void intDisplayDecSpace(uint16_t number);

void intDisplayHex(uint16_t number);

void intDisplayDecPoint(uint16_t number, uint8_t pointPosition);

#endif //_7SEG_H_
