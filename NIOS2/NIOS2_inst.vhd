	component NIOS2 is
		port (
			clk_clk          : in  std_logic                    := 'X';             -- clk
			led_export       : out std_logic_vector(3 downto 0);                    -- export
			reset_reset_n    : in  std_logic                    := 'X';             -- reset_n
			switch_export    : in  std_logic_vector(2 downto 0) := (others => 'X'); -- export
			display_readdata : out std_logic_vector(3 downto 0);                    -- readdata
			segment_readdata : out std_logic_vector(7 downto 0)                     -- readdata
		);
	end component NIOS2;

	u0 : component NIOS2
		port map (
			clk_clk          => CONNECTED_TO_clk_clk,          --     clk.clk
			led_export       => CONNECTED_TO_led_export,       --     led.export
			reset_reset_n    => CONNECTED_TO_reset_reset_n,    --   reset.reset_n
			switch_export    => CONNECTED_TO_switch_export,    --  switch.export
			display_readdata => CONNECTED_TO_display_readdata, -- display.readdata
			segment_readdata => CONNECTED_TO_segment_readdata  -- segment.readdata
		);

