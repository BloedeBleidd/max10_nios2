#ifndef KBD_H_
#define KBD_H_

#include <stdint.h>

//definicje alias�w dla przycisk�w
#define KBD_NONE	255
#define KBD_S1		0
#define KBD_S2		1
#define KBD_S3		2

#define KBD_PIO_BASE SW_BASE

#define KBD_S1_M (1<<0)
#define KBD_S2_M (1<<1)
#define KBD_S3_M (1<<2)


//minimalny czas dobouncingu (podawany w ms)
#define CLICK_MIN 50
//minimalny czas przytrzymania
#define HOLD_MIN 500
//powtarzanie po przytrzymaniu
#define HOLD_REP 70


//struktura elementu menu ze wska�nikiem na funkcj� oraz parametrem jej wywo�ania
typedef struct
{
	void (*action)(uint8_t);
	uint8_t parameter;
}menu_action_t;

//struktura uwzgl�dniaj��a akcj� dla klikni�cia i przytrzymania
typedef struct
{
	menu_action_t click;
	menu_action_t hold;
}menu_item_t;

//tabela struktur - dla ka�dego przycisku
menu_item_t menu_items[3];

void kbdCheck(void);


#endif /* KBD_H_ */
