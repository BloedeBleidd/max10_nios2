
module NIOS2 (
	clk_clk,
	led_export,
	reset_reset_n,
	switch_export,
	display_readdata,
	segment_readdata);	

	input		clk_clk;
	output	[3:0]	led_export;
	input		reset_reset_n;
	input	[2:0]	switch_export;
	output	[3:0]	display_readdata;
	output	[7:0]	segment_readdata;
endmodule
