//Dzieki temu naglowkowi mozemy korzystac ze zmiennych typu uint32_t itp.
#include <stdint.h>
#include <stdlib.h>
#include "system.h"
#include "sys/alt_stdio.h"
#include "sys/alt_sys_wrappers.h"
#include "altera_avalon_pio_regs.h"
#include "altera_avalon_timer_regs.h"
#include "sys/alt_irq.h"
#include "KBD/KBD.h"
#include "7SEG/7SEG.h"

volatile uint32_t cnt,cubeCnt;

void setLed( uint32_t p )
{
	IOWR_ALTERA_AVALON_PIO_DATA( LED_BASE, p );
}

void TIMER_INT(void* context)
{
	IOWR_ALTERA_AVALON_TIMER_STATUS(TIMER_BASE, 0);
	kbdCheck();
	setLed(~cnt);
	//intDisplayDecSpace(cnt);
	cubeCnt++;
}

void inc( uint8_t p )
{
	cnt++;
}

void dec( uint8_t p )
{
	cnt--;
}

void reset( uint8_t p )
{
	cnt = 0;
}

void cube( uint8_t p )
{
	srand(cubeCnt);
	intDisplayDecSpace( rand()%6+1 );
}

int main()
{ 
	alt_ic_isr_register(TIMER_IRQ_INTERRUPT_CONTROLLER_ID, TIMER_IRQ, TIMER_INT, NULL, NULL);

	IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_BASE, ALTERA_AVALON_TIMER_CONTROL_START_MSK |
		ALTERA_AVALON_TIMER_CONTROL_CONT_MSK | ALTERA_AVALON_TIMER_CONTROL_ITO_MSK );

	IOWR_32DIRECT(SEG7_BASE, 0x10, 500);
	IOWR_32DIRECT(SEG7_BASE, 0x14, 5000);

	menu_items[KBD_S1].click.action = inc;
	menu_items[KBD_S1].hold.action = inc;
	menu_items[KBD_S2].click.action = dec;
	menu_items[KBD_S2].hold.action = dec;
	menu_items[KBD_S3].click.action = reset;
	menu_items[KBD_S3].hold.action = cube;

	while (1);
	return 0;
}

